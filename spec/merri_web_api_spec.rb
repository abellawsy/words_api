# Spec File For Merri Web Api
require_relative '../helpers/merri_web_api'

RSpec.describe MerriWebApi do
  it 'can instantiate its instance variables' do
    test_merri = MerriWebApi.new
    expect(test_merri.url).to_not eq(nil)
    expect(test_merri.api_dict_key).to_not eq(nil)
    expect(test_merri.api_thes_key).to_not eq(nil)
  end

  it 'can call the merri web api' do
    test_merri = MerriWebApi.new
    result = test_merri.find_def('hello')
    expect(result.code).to eq('200')
  end
end
