# Test File For Words_API file.
ENV['RACK_ENV'] = 'test'

require 'rspec'
require 'rack/test'
require_relative '../words_api'

describe 'Words Api Application' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  it 'should allow accessing default page' do
    get '/'
    expect(last_response).to be_ok
    expect(last_response.body).to_not eq(nil)
  end

  it 'should allow accessing words page' do
    get '/words'
    expect(last_response).to be_ok
    expect(last_response.body).to_not eq(nil)
  end

  it 'should allow accessing books page' do
    get '/books'
    expect(last_response).to be_ok
    expect(last_response.body).to_not eq(nil)
  end
end
