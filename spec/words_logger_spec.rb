# Spec File For Words Logger Class
require_relative '../helpers/words_logger'

RSpec.describe WordsLogger do
  let(:file) { 'logs/specLog.log' }
  let(:test_logger) { WordsLogger.new(file) }

  it 'can instantiate its instance variables' do
    expect(test_logger.file_logger).to_not eq(nil)
    expect(test_logger.std_logger).to_not eq(nil)
  end

  it 'writes message to both file and std out' do
    test_logger.info('Hello Info Logger')
    test_logger.error('Hello Error Logger')
    test_logger.debug('Hello Debugger')

    # expect(file).to be_an_existing_file
  end
end
