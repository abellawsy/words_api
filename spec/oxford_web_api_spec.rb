# Spec File For Merri Web Api
require 'spec_helper'
require_relative '../helpers/oxford_web_api'

RSpec.describe OxfordWebApi do
  let(:test_oxf) { OxfordWebApi.new }

  it 'can instantiate its instance variables' do
    expect(test_oxf.url).to_not eq(nil)
    expect(test_oxf.api_id).to_not eq(nil)
    expect(test_oxf.api_key).to_not eq(nil)
  end

  it 'can call the oxford web api' do
    result = test_oxf.find_def('hello')
    expect(result.code).to eq('200')
  end
end
