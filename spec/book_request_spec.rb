# Spec File For Book Request Class
require 'spec_helper'
require_relative '../helpers/book_request'

RSpec.describe BookRequest do
  let(:bookNoTitle) { BookRequest.new }
  let(:bookFullInfo) do
    BookRequest.new('The Eye of the World', 'Robert Jordan', 'Tor Books',
                    '01/15/1990', '782', nil, '0812511816', '4.18')
  end
end
