# A Helper File To Help Process The Adding And Reading Of Books

require 'json'
require 'sinatra/activerecord'
require_relative 'words_logger'
require 'net/http'

class BookRequest
  attr_accessor :title, :author, :publisher, :publish_date, :page_count, :goodreads_id,
                :isbn, :rating, :book_body, :url, :logger
  # better route would be with array/hash as parameters.
  def initialize(title, author, pub, pub_date, pg_count, gr_id, isbn, rating)
    @title = title
    @author = author
    @publisher = pub
    @publish_date = pub_date
    @page_count = pg_count
    @goodreads_id = gr_id
    @isbn = isbn
    @rating = rating

    @logger = WordsLogger.new('logs/apiLog.log')
  end

  def initialize_with_params(params)
    unless params[:title].nil? || params[:author].nil?
      initialize(params[:title], params[:author], params[:pub],
                 params[:pub_date], params[:pg_count], params[:gr_id],
                 params[:isbn], params[:rating])
    end
  end

  def build_book_request
    if @title.nil? || @author.nil?
      @book_body = { message: 'Cannot Add A Book Without Title Or Author' }
    else
      @book_body = {
        title: @title, author: @author, publisher: @publisher,
        publish_date: @publish_date, page_count: @page_count,
        goodreads_id: @goodreads_id, isbn: @isbn, rating: @rating
      }
    end
    @book_body.to_json
  end

  def send_book_request(uri = nil)
    if uri.nil?
      credentials = Psych.load_file('config.yml')
      @url = credentials['local']['url']
    else
      @url = uri
    end

    encoded_url = URI.encode(@url + '/book')
    uri = URI(encoded_url)
    req = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
    req.body = build_book_request

    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end

    puts 'Need To Raise Exception Here' if res.body.nil?

    res
  end
end

#
#
# uri = URI('https://myapp.com/api/v1/resource')
# req = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
# req.body = {param1: 'some value', param2: 'some other value'}.to_json
# res = Net::HTTP.start(uri.hostname, uri.port) do |http|
#   http.request(req)
# end
#
#
#
# =
