# The Code To Parse The XML Return From Different APIs.
require 'nokogiri'

class XmlParser
  def initialize; end

  # Internal Helper Method To Parse XML Return From Merri Web API
  def merri_web_def_xml_parser(xml)
    doc = Nokogiri::XML(xml)

    # Word
    word = doc.xpath('//et')
    # Noun/Adjective etc...
    word_type = doc.xpath('//fl')
    # Actual Word Definition
    word_definition = doc.xpath('//dt')

    word_string = "#{word} (#{word_type}): #{word_definition}"

    word_string
  end
end
