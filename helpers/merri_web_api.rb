require 'nokogiri'
require 'net/http'
require 'psych'

require_relative 'xml_parser'
require_relative 'words_logger'

# The class to handle the interaction with the merriWeb API
class MerriWebApi
  attr_accessor :url, :api_dict_key, :api_thes_key

  def initialize
    credentials = Psych.load_file('config.yml')
    @url = credentials['merriWeb']['url']
    @api_dict_key = credentials['merriWeb']['dictKey']
    @api_thes_key = credentials['merriWeb']['thesKey']
    @xml_parse = XmlParser.new
  end

  # Method to Call Merri Web API TO Define Words
  def find_def(word)
    encoded_url = URI.encode(@url + word + '?key=' + @api_dict_key)
    uri = URI(encoded_url)
    res = Net::HTTP.get_response(uri)
    if !res.body.nil?
      @xml_parse.merri_web_def_xml_parser(res.body)
    else
      puts 'Need To Raise Exception Here'
    end
    res
  end
end
