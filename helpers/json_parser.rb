# Code to handle parsing of json
require 'json'
require 'pp'

require_relative '../models/word'
# Class To handle Json Parsing Of Api Returns
class JsonParser
  def initialize; end

  def oxf_api_parser(res)
    json_array = JSON.parse(res)
    ret_hash = {}
    # Source
    ret_hash['Source'] = json_array['metadata']['provider']
    # Words
    ret_hash['Word'] = json_array['results'][0]['id']
    # Type of Word
    ret_hash['Lexical Category'] = json_array['results'][0]['lexicalEntries'][0]['lexicalCategory']
    # Definition 
    ret_hash['Definition'] = json_array['results'][0]['lexicalEntries'][0]['entries'][0]['senses'][0]['definitions'][0] 
    
    pp ret_hash

    # Save TO DB
    
  end
end

test_json = '{ "metadata": { "provider": "Oxford University Press" }, "results": [ { "id": "sartorial", "language": "en", "lexicalEntries": [ { "entries": [ { "etymologies": [ "early 19th century: from Latin sartor ‘tailor’ (from sarcire ‘to patch’) + -ial" ], "grammaticalFeatures": [ { "text": "Positive", "type": "Degree" } ], "homographNumber": "000", "senses": [ { "definitions": [ "relating to tailoring, clothes, or style of dress" ], "domains": [ "Clothing" ], "examples": [ { "text": "sartorial elegance" } ], "id": "m_en_gbus0901040.005", "notes": [ { "text": "attributive", "type": "grammaticalNote" } ] } ] } ], "language": "en", "lexicalCategory": "Adjective", "pronunciations": [ { "audioFile": "http://audio.oxforddictionaries.com/en/mp3/sartorial_gb_1_8.mp3", "dialects": [ "British English" ], "phoneticNotation": "IPA", "phoneticSpelling": "sɑːˈtɔːrɪəl" } ], "text": "sartorial" } ], "type": "headword", "word": "sartorial" } ] }'

parser = JsonParser.new

parser.oxf_api_parser(test_json)
