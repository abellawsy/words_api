# Custom Logger File - STD OUT and Write To File - Extends Logger
require 'logger'

class WordsLogger
  attr_accessor :file_logger, :std_logger
  def initialize(fileName)
    @file_logger = Logger.new(fileName, 'monthly')
    @std_logger = Logger.new(STDOUT, 'monthly')
  end

  def info(message)
    @file_logger.info(message)
    @std_logger.info(message)
  end

  def error(message)
    @file_logger.error(message)
    @std_logger.error(message)
  end

  def debug(message)
    @file_logger.debug(message)
    @std_logger.debug(message)
  end
end
