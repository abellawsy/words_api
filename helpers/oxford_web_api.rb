require 'json'
require 'net/https'
require 'psych'

require_relative 'words_logger'

# The Class File To Handle Oxford Web Api Interactions
class OxfordWebApi
  attr_accessor :url, :api_key, :api_id, :logger

  def initialize
    credentials = Psych.load_file('config.yml')
    @url = credentials['oxford']['url']
    @api_id = credentials['oxford']['id']
    @api_key = credentials['oxford']['key']

    @logger = WordsLogger.new('logs/apiLog.log')
  end

  # Method to Find Definitions Using Oxford Api
  def find_def(word)
    url = URI.parse(@url + word.downcase)

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    req = Net::HTTP::Get.new(url)
    req.add_field('app_id', @api_id)
    req.add_field('app_key', @api_key)
    logger.info("Send HTTP Req - #{url}")

    res = http.request(req)

    return res unless res.nil?
    puts 'Invalid Api Return From - Oxford Web Api'
  end
end
