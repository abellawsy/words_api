# Ruby Class To Test Scripts.
require_relative 'helpers/merri_web_api'
require_relative 'helpers/oxford_web_api'
require_relative 'helpers/book_request'
require_relative 'helpers/json_parser'

# Instantiate MerriWebApi Class
merri_api = MerriWebApi.new

# Instantiate MerriWebApi Class
oxford_api = OxfordWebApi.new

# Instantiate Book Request Class
# bookEmpty = BookRequest.new
bookFilled = BookRequest.new('The Eye of the World', 'Robert Jordan', 'Tor Books', '01/15/1990',
                             '782', nil, '0812511816', '4.18')

# Json Parser Class
jsonPar = JsonParser.new

# ARGV Declaration
arguments = ARGV

if arguments.first.nil?
  puts 'Empty Argument List'
  puts '-------------------------------'
  puts 'Building a Request for an Empty Book '
  # puts bookEmpty.build_book_request()
  puts '-------------------------------'
  puts 'Building a Request for a Full Book '
  puts bookFilled.build_book_request
  puts bookFilled.send_book_request('https://words-api-reconstructional-cataloguist.cfapps.io').body

else
  # puts "Merriam Webster Definition"
  # merri_api.find_def(arguments.first)
  puts '-------------------------------'
  puts 'Oxford Dictionary Definition'
  results = oxford_api.find_def(arguments.first)
  puts jsonPar.oxf_api_parser(results.body)

end
