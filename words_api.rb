# The Actual Api File For Words For Readers
require 'sinatra'
require_relative 'helpers/words_logger'
require_relative 'helpers/oxford_web_api'
require_relative 'helpers/json_parser'
require 'json'

# Configure Application To Use Puma
configure { set :server, :puma }

logger = WordsLogger.new('logs/apiLog.log')
api = OxfordWebApi.new
parser = JsonParser.new # Has debug code to remove

# Way to include all models
current_dir = Dir.pwd
Dir["#{current_dir}/models/*.rb"].each { |file| require file }

# Setting Up Sinatra Config
port_num = ENV['port'].to_i
set :port, port_num if (port_num != 0) && !port_num.nil?
set :bind, '0.0.0.0'

# ------------------ Get Section -------------------------------------
get '/' do
  'Welcome To Words For Readers'
end

get '/books' do
  book = Book.all
  'Look At All These Books I Have Read!! \n' + JSON.pretty_generate(book.to_json)
end

get '/books/:title' do
  @book = Book.find(params[:title])
  'I Read This Book:' + JSON.pretty_generate(book.to_json)
end

get '/words' do
  @word = Word.all
  'Look At All These Words I Now Know!!'
end

get '/words/:word' do
  @word = api.find_def(params[:word])
  response = parser.oxf_api_parser(@word.body)
  "Look At This API Return - #{response}"
end

#--------------------  Post Section ----------------------------------------
post '/word' do
  if request.body.nil?
    logger.info('Blank Post Request to /word!!!')
    'Blank Post Request to /word!!!'
  else
    request.body.rewind
    data = JSON.parse request.body.read
    logger.info "/word http Request - #{data}"
    'Sending A Post Request To Words'
  end
end

post '/book' do
  if request.body.rewind.nil?
    logger.info('Blank Post Request to /book!!!')
    'Blank Post Request to /book!!!'
  else
    request.body.rewind
    data = JSON.parse request.body.read
    puts data.to_s
    logger.info("/book http Request with body - #{data}")
    "Received Post Request To /book endpoint with body - #{data}"
  end
end
