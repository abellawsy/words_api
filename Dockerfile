#Docker File For The Change Of Heart Safar Vue On Rails App
# Ruby File uses Debian Jessie as the OS. 
FROM ruby:2.4

#Update OS
RUN apt-get update -y && apt-get upgrade -y

#Expose the Port Needed For Sails
EXPOSE 4567

# Make An Application Folder
RUN mkdir /words_Api

# Make An App Directory For Safar
WORKDIR /words_Api

# Copy Local Directory Into Container
COPY . .

#Install Packages
RUN bundle install

#Command To Start Rails Application
CMD ruby words_api.rb