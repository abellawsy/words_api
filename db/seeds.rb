# File To seed Databases.
require_relative '../models/book'
book_list = [
  ['The Eye of the World', 'Robert Jordan', 'Tor Books', '01/15/1990', '782', nil, '0812511816', '4.18'],
  ['The Great Hunt', 'Robert Jordan', 'Tor Books', '11/15/1990', '705', nil, '0812517725', '4.2'],
  ['The Dragon Reborn', 'Robert Jordan', 'Tor Books', '10/15/1991', '624', nil, '0765305119', '4.23'],
  ['The Shadow Rising', 'Robert Jordan', 'Tor Books', '09/15/1992', '1007', nil, '0812513738', '4.21'],
  ['The Fires Of Heaven', 'Robert Jordan', 'Tor Books', '10/15/1993', '912', nil, '1857232097', '4.13']
]

book_list.each do |title, author, publ, pub_date, pg_count, goodreads, bn, rate|
  Book.create(title: title, author: author, publisher: publ, orig_date_publ: pub_date,
              page_count: pg_count, goodreads_id: goodreads, isbn: bn, rating: rate)
end
