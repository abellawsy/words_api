class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :title, null: false
      t.string :author, null: false
      t.string :publisher
      t.date :orig_date_publ
      t.integer :page_count
      t.integer :goodreads_id
      t.integer :isbn
      t.decimal :rating

      t.timestamps
    end
  end
end
