class CreateWords < ActiveRecord::Migration[5.1]
  def change
    create_table :words do |t|
      t.string :word, null: false
      t.string :def_1, null: false
      t.string :def_2
      t.string :def_3
      t.string :etymo
      t.string :examples

      t.timestamps
    end
    add_index(:words, :word)
  end
end
